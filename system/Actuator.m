classdef Actuator
    properties
        State {mustBeNumeric}
        Mode {mustBeNumeric}
    end
    methods
        function obj = init(obj)
            obj.State = 1;
            HALF = 0;
            obj.Mode = HALF;
        end
        function obj = next_state(obj,act,mode)
            obj.State = act;
            obj.Mode = mode;
        end
        function r = get_value(obj)
            r = obj.State;
        end
        function r = get_mode(obj)
            r = obj.Mode;
        end
        
        function r = get_state(obj)
            r = zeros(1,2);
            r(1) = obj.State;
            r(2) = obj.Mode;
        end
    end
end