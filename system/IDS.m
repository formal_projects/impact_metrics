classdef IDS
    properties
        State 
        Actuator
        Mode
    end
    methods
        function obj = init(obj)
            OK = 0;
            HALF = 0;
            FULL = 1;
            obj.State = OK;
            obj.Actuator = 1;
            obj.Mode = HALF;
        end
       function r = get_alarm(obj)
           OK = 0;
            if obj.State == OK
                r = 0;
            else
                r = 1;
            end
        end
        function r = get_state(obj)
            r = obj.State;
        end
        function r = get_command(obj)
            r = obj.Actuator;
        end
        function r = get_ins(obj)
            r = obj.Mode;
        end
        function obj = next_state(obj, measurement,command)
            OK = 0;
            HOT = 1;
            HALF = 0;
            FULL = 1;
            SLOW = -1;
            if measurement > 100 &&  command > 0
                obj.State = HOT;
                obj.Mode = SLOW;
                %SEND FULL
            else
                obj.State = OK;
                obj.Mode = HALF;
            end
            obj.Actuator = command;
        end
    end
end