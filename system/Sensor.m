classdef Sensor
    properties
        value
    end
    methods
        function obj = init(obj)
            obj.value = 0;
        end
        function r = get_value(obj)
         r = obj.value;
        end
        function r = get_state(obj)
            r = obj.value;
        end
        function obj = next_state(obj,state,error)
            obj.value = state + error;
        end
    end
end