classdef Controller
    properties
        State 
        Actuator 
        insMode
        reqMode
        Mode
    end
    methods
        function obj = init(obj)
            HALF = 0;
            obj.State = 0;
            obj.Actuator = 1;
            obj.Mode = HALF;
        end
        function r = get_state(obj)
            r = obj.State;
        end
        function r = get_cmd(obj)
            r = obj.Actuator;
        end
        function r = get_mode(obj)
            r = obj.Mode;
        end
        function obj = next_state(obj, measurement,ins_mode, req_mode)
            SLOW = -1;
            HALF = 0;
            FULL = 1;
            switch obj.State
                case 0
                    if (measurement > 100)
                        obj.Actuator = -1;
                        obj.State = 1;
                    else
                        obj.Actuator = 1;
                        obj.State = 0;
                        
                        if(ins_mode == SLOW)
                            obj.Mode = SLOW;
                        else
                            obj.Mode = req_mode;
                        end
                    end
                case 1
                    obj.Actuator = obj.Actuator;
                    obj.State = 2;
                case 2
                    obj.Actuator = obj.Actuator;
                    obj.State = 3;
                case 3
                    obj.Actuator = obj.Actuator;
                    obj.State = 4;
                case 4%%CHECK
                    obj.Actuator = obj.Actuator;
                    obj.State = 0;
                    if(ins_mode == SLOW)
                        obj.Mode = SLOW;
                    else
                        obj.Mode = req_mode;
                    end
                        
                otherwise
                    obj.State = 0;
                    obj.Actuator = obj.Actuator;
            end
        end
    end
end