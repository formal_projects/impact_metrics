classdef CPS
    properties
        controller
        ids
        plant
        sensor
        actuator
        stress
        Mode {mustBeNumeric}
    end
    methods
        function obj = CPS
            obj.controller = Controller;
            obj.ids = IDS;
            obj.plant = Plant;
            obj.sensor = Sensor;
            obj.actuator = Actuator;
            obj.stress = Stress;
            obj.controller = obj.controller.init();
            obj.ids = obj.ids.init();
            obj.plant = obj.plant.init();
            obj.sensor = obj.sensor.init();
            obj.actuator = obj.actuator.init();
            obj.stress = obj.stress.init();
            HALF = 0;
            obj.Mode = HALF;
        end
        function obj = update_logics(obj,req_mode)
            obj.controller = obj.controller;
            obj.ids = obj.ids;
            obj.plant = obj.plant;
            obj.sensor = obj.sensor;
            obj.actuator = obj.actuator;
            obj.stress = obj.stress;
            
            measurement= obj.sensor.get_value();
            ins_mode = obj.ids.get_ins();
            % update controller
            obj.controller = obj.controller.next_state(measurement,ins_mode, req_mode);
            %update_controler
            act_cmd = obj.controller.get_cmd();
            act_mode = obj.controller.get_mode();
            obj.actuator = obj.actuator.next_state(act_cmd,act_mode);
            obj.ids = obj.ids.next_state(measurement,act_cmd);
        end
        
        function obj = update_physics(obj,uncert)
            obj.controller = obj.controller;
            obj.ids = obj.ids;
            obj.plant = obj.plant;
            obj.sensor = obj.sensor;
            obj.actuator = obj.actuator;
            obj.stress = obj.stress;
            
            act_value = obj.actuator.get_value();
            act_mode = obj.actuator.get_mode();
            % updated plant
            obj.plant = obj.plant.next_state(act_value,act_mode, uncert);
            state = obj.plant.get_state();
            % update stress
            obj.stress = obj.stress.next_state(state);
            % update sensor
            error = 0;
            obj.sensor = obj.sensor.next_state(state,error);
        end
        function r = get_state(obj)
           actuator_state = obj.actuator.get_state();
           ids_state = obj.ids.get_state();
           controller_state = obj.controller.get_state();
           r = [ids_state, obj.plant.get_state(), actuator_state, obj.stress.get_state()]; 
        end
         function r = get_stress(obj)
           r = obj.stress.get_stress(); 
         end
         function r = get_mode(obj)
            r = obj.Mode;
         end
         function r = get_alarm(obj)
           r = obj.ids.get_alarm(); 
         end
         function r = get_uncert(obj)
            SLOW = -1;
            HALF = 0;
            FULL = 1;
            uncert_full = -0.2:0.1:0.2;
            uncert_half = -0.1:0.1:0.2;
            uncert_slow = -0.1:0.1:0.1;
            act_cmd = obj.controller.get_cmd();
            act_mode = obj.controller.get_mode();
            if (act_cmd < 0)
                uncert = uncert_full;
            else
                if(act_mode == SLOW)
                    uncert = uncert_slow;
                elseif(act_mode == HALF)
                    uncert = uncert_half;
                else
                    uncert = uncert_full;
                end
            end
            r = uncert;
         end
             
    end
end