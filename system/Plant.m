classdef Plant
    properties
        State {mustBeNumeric}
        Mode {mustBeNumeric}
        Act
    end
    methods
        function obj = init(obj)
            SLOW = -1;
            HALF = 0;
            FULL = 1;
            obj.State = 95;
            obj.Mode = HALF;
        end
        function obj = next_state(obj, act, mode, uncert)
            SLOW = -1;
            HALF = 0;
            FULL = 1;
            obj.Act = act;
            obj.Mode = mode;
            if(act > 0) 
                if (obj.Mode == SLOW)
                    act = 0.2;
                elseif(obj.Mode == HALF)
                    act = 0.5;
                end
            end
            aux = obj.State + act + uncert;
            obj.State = aux; 
        end
        function r = get_mode(obj)
            r = obj.Mode;
        end
        function r = get_lambda(obj)
            if (obj.Mode == SLOW)
                aux = act_cmd;
            elseif(obj.Mode == HALF)
                aux = obj.State + act + uncert;
            else
                aux = obj.State + act + uncert;
            end
            r = aux;
        end
        function r = get_state(obj)
            r = obj.State;
        end
    end
end