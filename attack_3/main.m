clear all;
close all;
clc
cps = CPS_Attack3;
global states_store;
global results;
results = {};
states_store = containers.Map;
MAX = 52
time_windows = 1:5:MAX;
tic
res = run_simulation_3(cps,time_windows,[]);
timeElapsed = toc
fn = res(:,1);
fp = res(:,2);

figure
plot(time_windows,fn)
hold on
plot(time_windows,fp)
ylim([0 1]);
xlim([0 MAX-2]);
grid on
xticks(1:50:MAX)
xlabel('Time');
legend({'$\mathbf{FN}^n_{{\mathcal I},{{\mathcal S}}}(Eng)$','$\mathbf{FP}^n_{{\mathcal I},S}(\mathit{Eng})$'},'Interpreter','latex');


