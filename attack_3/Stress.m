classdef Stress
    properties
        State {mustBeNumeric}
        Past_temps
        Stress_value
    end
    methods
        function obj = init(obj)
            obj.State = 0;
            MAX_PAST = 6;
            obj.Past_temps = ones(1,MAX_PAST)*90;
            obj.Stress_value = 0;
        end
        function obj = next_state(obj, temp)
            MAX_STRESS = 50;
            obj.Past_temps = obj.Past_temps(2:end);
            obj.Past_temps(end+1) = temp;
           
%             indices2 = find(obj.Past_temps > 100);
%             if (length(indices2) >= 2)
%                 obj.Past_temps
%             end
            indices = find(obj.Past_temps < 50 | obj.Past_temps > 100);
            MAX_INDICES = 4;
            incr = length(indices) >= MAX_INDICES;

            obj.Stress_value = obj.Stress_value + incr;
            obj.State = min(1, obj.State + obj.Stress_value/MAX_STRESS);  
        end
        function r = get_state(obj)
            r = [obj.Stress_value,(obj.Past_temps < 50 & obj.Past_temps > 100)];
        end
        function r = get_stress(obj)
            r = obj.State;
        end
    end
end