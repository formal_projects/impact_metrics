%%%%%%%%%%%%%%%
% This computes fp and fn for each time_window in time_windows
%%%%%%%%%%%%%%%
function [return_value] = run_simulation_3(system, time_windows, data_vector)
s_state = system.get_state();

temp_index = 2;
%contains the temperature
%data_vector(end+1) = s_state(temp_index);
HALF = 0;
FULL = 1;
req_ins  = [FULL,HALF]; %[HALF,FULL];
global states_store;
current_state = system.get_state();

size_tm = length(time_windows);
empty_state = zeros(size_tm,2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % index_2 = 4;
% % index_3 = 1;
% % mode = s_state(index_2);
% % ids_state = s_state(index_3);
% % plot(0:1:length(data_vector)-1, data_vector);
% % hold on
% % plot(length(data_vector)-1, 5*mode,'o')
% % hold on
% % plot(length(data_vector)-1, 6*ids_state-2,'x')
% % hold on
% hold on
% if (time_windows(end) <= 0)
%     plot(0:1:length(data_vector)-1, data_vector);
%     hold on
% end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if isKey(states_store,mat2str([current_state(:);time_windows(end)]))
    aux = states_store( mat2str([current_state(:);time_windows(end)]));
    return_value = aux;
    return
end

zero_index = find(time_windows == 0);
positive_index = length(find(time_windows > 0));
len_zero_index = length(zero_index);

if positive_index == 0 && len_zero_index == 0
    states_store( mat2str([current_state(:);time_windows(end)])) = empty_state;
    return;
elseif len_zero_index == 1 && positive_index == 0
    aux = empty_state;% states_store( mat2str([current_state(:);time_windows(end)]));
    fn = 0;
    fp = 0;
    aux(zero_index,:) = [fn,fp];
    states_store( mat2str([current_state(:);time_windows(end)])) = aux;
    return_value = aux;
    return 
else
  
    k = 0;
    values_to_compare_1 = [];
    values_to_compare_2 = [];
     
   for req_in=req_ins
        k = k+1;
        fn_js = [];
        fp_js = [];
        system_temp = system.update_logics(req_in);
        uncert = system_temp.get_uncert();
%         current_state = system_temp.get_state();
        
        % to get that, I need to know the actuation
        for j=uncert
            system_copy = system_temp.update_physics(j);
            %next_state = system_copy().get_state();
            aux_return_value = run_simulation_3(system_copy, time_windows-1,data_vector);
            fns = aux_return_value(:,1);
            fps = aux_return_value(:,2);
            fn_js = [fn_js,fns];
            fp_js = [fp_js,fps];
        end
        q = system.get_alarm();
        i = system.get_stress();
        r_js = 1;
        i_js = i;
        q_js = q;
        
        
        gamma = 1/length(uncert);
        gammas = ones(1,length(uncert))*gamma;
        
       
        stored_values_aux = empty_state;
        
        for i=1:length(time_windows)
            if time_windows(i) > 0
                fn_vec = fn_js(i,:)';
                fp_vec = fp_js(i,:)';

                fn = min(1, (r_js*max(0,i_js - q_js) + (time_windows(i)-1)*gammas*fn_vec)/time_windows(i));
                fp = min(1, (r_js*max(0,q_js - i_js) + (time_windows(i)-1)*gammas*fp_vec)/time_windows(i));
                stored_values_aux(i,:) = [fn,fp];
            elseif time_windows(i) == 0
                stored_values_aux(i,:) = [0,0];
            end
        end
        if(k == 1) 
            values_to_compare_1 = stored_values_aux;
%             values_to_compare_2 = stored_values_aux;
        else
            values_to_compare_2 = stored_values_aux;
        end
        
    end
   for i=1:length(time_windows)
        if time_windows(i) > 0
            aux_ff_1 = values_to_compare_1(i,:);
            fn_1 = aux_ff_1(1);
            fp_1 = aux_ff_1(2);
            aux_ff_2 = values_to_compare_2(i,:);
            fn_2 = aux_ff_2(1);
            fp_2 = aux_ff_2(2);
            stored_values_aux(i,:) = [max(fn_1,fn_2),max(fp_1,fp_2)];
        elseif time_windows(i) == 0
            stored_values_aux(i,:) = [0,0];
        end
%         or 
%         
%         
%         
   end
%     stored_values_aux= pmax(values_to_compare_1, values_to_compare_2)
    states_store( mat2str([current_state(:);time_windows(end)])) = stored_values_aux;
    return_value = stored_values_aux;
end

end